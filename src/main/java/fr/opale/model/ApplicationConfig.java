package fr.opale.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import routines.system.PasswordEncryptUtil;

import java.io.Serializable;

/**
 * Created by thomas.croguennec on 20/10/16.
 */
@Slf4j
@NoArgsConstructor
public class ApplicationConfig implements Serializable {

    @Getter
    @Setter
    private String pathToOPALE;

    // CONFIG DATASOURCE OPALE
    @Getter
    @Setter
    private String urlOPALE;

    @Getter
    @Setter
    private String usernameOPALE;

    @Getter
    private String passwordOPALE;

    @Getter
    private String driverOPALE = "com.microsoft.sqlserver.jdbc.SQLServerDriver";

    @Getter
    @Setter
    private String portOPALE;

    @Getter
    @Setter
    private String databaseOPALE;

    @Getter
    private String schemaOPALE = "dbo";

    // CONFIG DATASOURCE ENI
    @Getter
    @Setter
    private String urlENI;

    @Getter
    @Setter
    private String usernameENI;

    @Getter
    private String passwordENI;

    @Getter
    private String driverENI = "name=com.microsoft.sqlserver.jdbc.SQLServerDriver";

    @Getter
    @Setter
    private String portENI;

    @Getter
    @Setter
    private String databaseENI;

    @Getter
    private String schemaENI = "dbo";


    // CONFIG DATASOURCE API
    @Getter
    private String hibernateDDL = "none";

    @Getter
    private String databasePlatform = "org.hibernate.dialect.SQLServerDialect";

    @Getter
    private String maxWait = "10000";

    @Getter
    private String maxActive = "8";

    @Getter
    private String testWhileIdle = "true";

    @Getter
    private String testOnBorrow = "false";

    @Getter
    private String connectionTest = "SELECT 1";

    @Getter
    private String jpaDialect = "org.hibernate.dialect.SQLServerDialect";

    @Getter
    private String loggingConfig = "classpath:logback-spring.xml";

    @Getter
    private String loggingErrorPageFilter = "OFF";

    // CONFIG LDAP
    @Getter
    @Setter
    private String urlLdap = "ldap://127.0.0.1";

    @Getter
    @Setter
    private String dc = "OU=ENI Ecole,DC=eni,DC=local";

    @Getter
    @Setter
    private String dn = "eni";

    // CONFIG ETL
    @Getter
    private String passwordOPALEEncrypted;

    @Getter
    private String passwordENIEncrypted;

    @Getter
    private String opaleHabilitationFile = "src/main/resources/etl/opale-habilitations.properties";

    @Getter
    private String opaleParamFile = "src/main/resources/etl/opale-parametrage.properties";

    @Getter
    private String opaleConfigFile = "src/main/resources/etl/opale-configuration.properties";

    public void setPasswordOPALE(String passwordOPALE) {
        this.passwordOPALE = passwordOPALE;
        try {
            this.passwordOPALEEncrypted = PasswordEncryptUtil.encryptPassword(passwordOPALE);

        } catch (Exception ex) {
            log.error("Erreur d'encryptage du password : {}", ex.getLocalizedMessage());
        }
    }

    public void setPasswordENI(String passwordENI) {
        this.passwordENI = passwordENI;
        try {
            this.passwordENIEncrypted = PasswordEncryptUtil.encryptPassword(passwordENI);
        } catch (Exception ex) {
            log.error("Erreur d'encryptage du password : {}", ex.getLocalizedMessage());
        }
    }
}
