package fr.opale.controller;

import fr.opale.model.ApplicationConfig;
import fr.opale.util.SQLUtils;
import fr.opale.util.Utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.sql.SQLException;

/**
 * Created by thomas.croguennec on 20/10/16.
 */
@Slf4j
@Controller
public class HomeController {

    @Value("${spring.datasource.url}")
    String url;
    @Value("${spring.datasource.username}")
    String user;
    @Value("${spring.datasource.password}")
    String password;
    @Value("${spring.datasource.driver-class-name}")
    String driver;


    @RequestMapping("/")
    public String index(Model model) {
        model.addAttribute("config", new ApplicationConfig());
        return "index";
    }

    @RequestMapping(value = "/sendForm", method = RequestMethod.POST)
    public String sendForm(Model model, @ModelAttribute ApplicationConfig config){
        model.addAttribute("erreur", false);

        log.info("#########################################################");
        log.info("PARAMETRAGE :: DEBUT !");
        log.info("#########################################################");

        Utils.writeFileDbInstallConf(config, "src/main/resources/application.properties");

        log.info("#########################################################");

        Utils.writeFileDbConf(config, config.getPathToOPALE() + "/src/main/resources/application.properties");

        log.info("#########################################################");

        Utils.writeFileETLConf(config, config.getPathToOPALE() + "/src/main/resources/etl/Default.properties");

        log.info("#########################################################");

        Utils.writeFileLDAPConf(config, config.getPathToOPALE() + "/src/main/resources/ldap.properties");

        log.info("#########################################################");
        log.info("PARAMETRAGE :: CREATION DE LA BASE !");
        log.info("#########################################################");

        SQLUtils sqlUtils = new SQLUtils();

        try{
            sqlUtils.executeDDL(url, user, password);
        } catch(SQLException ex) {
            log.error(ex.getLocalizedMessage());
            String exception = ex.getLocalizedMessage();
            model.addAttribute("erreur", true);
        }

        try{
            log.info("PARAMETRAGE :: AJOUT DU USER JGABILLAUD !");
            sqlUtils.addAdminAccount(url, user, password, driver);
        } catch(SQLException ex) {
            log.error(ex.getLocalizedMessage());
            String exception = ex.getLocalizedMessage();
            model.addAttribute("erreur", true);
        } finally {
            log.info("#########################################################");
            log.info("PARAMETRAGE :: FIN !");
            log.info("#########################################################");
        }

        return "resultForm";

    }
}
