package fr.opale.util;


import lombok.extern.slf4j.Slf4j;
import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.init.ScriptUtils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * installOpale - fr.opale.util
 * <p>
 * Created by kro on 20/10/16.
 * On 20/10/16
 */
@Slf4j
public class SQLUtils {

    /**
     * Exexcute SQL file
     * @param url the url of the target database
     * @param user the username of the target database
     * @param password the password of the target database
     * @throws SQLException the exception to throw
     */
    public void executeDDL(String url, String user, String password) throws SQLException{
        Connection connection = DriverManager.getConnection(url, user, password);
        ScriptUtils.executeSqlScript(connection, new ClassPathResource("opale_create.sql"));
        connection.close();
    }

    /**
     * Insert into table FORMATEUR the user JGABILLAUD
     * @param url the url of the target database
     * @param user the username of the target database
     * @param password the password of the target database
     * @param driver the driver of the target database
     * @throws SQLException the exception to throw
     */
    public void addAdminAccount(String url, String user, String password, String driver) throws SQLException{
        log.info("Création du user .................");
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(driver);
        dataSource.setUrl(url);
        dataSource.setUsername(user);
        dataSource.setPassword(password);
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("insert into Habilitation(libellehabilitation) values(?)", "admin");
        jdbcTemplate.update("insert into Formateur(idformateur, isexterne, mail, nom, prenom, username, idhabilitation) values(?, ?, ?, ?, ?, ?, ?)",
                15, 0, "jgabillaud@eni.fr", "GABILLAUD", "Jérôme", "jgabillaud", 1);
    }

}
