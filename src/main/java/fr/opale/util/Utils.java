package fr.opale.util;

import fr.opale.model.ApplicationConfig;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by thomas.croguennec on 20/10/16.
 */
@Slf4j
public class Utils {

    /**
     * Write installation db conf
     * @param config applicationConfig
     * @param filename the file to write
     */
    public static void writeFileDbInstallConf(ApplicationConfig config, String filename) {
        File dbConfFile = new File(filename);

        try {
            log.info("Création du fichier de conf d'installation");
            dbConfFile.createNewFile();

            log.info("Ecriture du fichier");
            FileWriter writer;
            writer = new FileWriter(dbConfFile);

            log.info("Ouverture du buffer! Ecriture en cours.....");

            BufferedWriter bw = new BufferedWriter(writer);
            bw.write("# ===============================\n");
            bw.write("# = DATA SOURCE\n");
            bw.write("# ===============================\n");
            bw.write("spring.datasource.url=jdbc:sqlserver://" + config.getUrlOPALE() + ":" + config.getPortOPALE() + ";databaseName=" + config.getDatabaseOPALE() + "\n");
            bw.write("spring.datasource.username=" + config.getUsernameOPALE() + "\n");
            bw.write("spring.datasource.password=" + config.getPasswordOPALE() + "\n");
            bw.write("spring.datasource.driver-class-name=" + config.getDriverOPALE() + "\n");
            bw.write("spring.thymeleaf.cache=false \n");
            bw.write("spring.datasource.initialize=false \n");
            bw.write("spring.datasource.continueOnError=true \n");
            bw.write("logging.config=" + config.getLoggingConfig() + "\n");
            bw.close();

            log.info("Fermeture du fichier !");
        } catch (IOException e) {
            log.error("Une erreur est survenue : {}", e.getLocalizedMessage());
        }
    }

    /**
     * Write opale db conf
     * @param config applicationConfig
     * @param filename the file to write
     */
    public static void writeFileDbConf(ApplicationConfig config, String filename) {
        File dbConfFile = new File(filename);

            try {
                log.info("Création du fichier");
                dbConfFile.createNewFile();

                log.info("Ecriture du fichier");
                FileWriter writer;
                writer = new FileWriter(dbConfFile);

                log.info("Ouverture du buffer! Ecriture en cours.....");

                BufferedWriter bw = new BufferedWriter(writer);
                bw.write("# ===============================\n");
                bw.write("# = DATA SOURCE\n");
                bw.write("# ===============================\n");
                bw.write("spring.datasource.url=jdbc:sqlserver://" + config.getUrlOPALE() + ":" + config.getPortOPALE() + ";databaseName=" + config.getDatabaseOPALE() + "\n");
                bw.write("spring.datasource.username=" + config.getUsernameOPALE() + "\n");
                bw.write("spring.datasource.password=" + config.getPasswordOPALE() + "\n");
                bw.write("spring.datasource.driver-class-name=" + config.getDriverOPALE() + "\n");
                bw.write("spring.datasource.max-wait=" + config.getDatabasePlatform() + "\n");
                bw.write("spring.datasource.max-active=" + config.getMaxActive() + "\n");
                bw.write("spring.datasource.test-while-idle=" + config.getTestWhileIdle() + "\n");
                bw.write("spring.datasource.test-on-borrow=" + config.getTestOnBorrow() + "\n");
                bw.write("spring.datasource.connection-test-query=" + config.getConnectionTest() + "\n");
                bw.write("# ===============================\n");
                bw.write("# = JPA / HIBERNATE\n");
                bw.write("# ===============================\n");
                bw.write("spring.jpa.hibernate.ddl-auto=" + config.getHibernateDDL() + "\n");
                bw.write("spring.jpa.hibernate.dialect=" + config.getJpaDialect() + "\n");
                bw.write("# ===============================\n");
                bw.write("# = LOGGING\n");
                bw.write("# ===============================\n");
                bw.write("logging.config=" + config.getLoggingConfig() + "\n");
                bw.write("logging.level.org.springframework.boot.context.ErrorPageFilter=" + config.getLoggingErrorPageFilter() + "\n");
                bw.close();

                log.info("Fermeture du fichier !");
            } catch (IOException e) {
                log.error("Une erreur est survenue : {}", e.getLocalizedMessage());
            }
    }

    /**
     * Write ETL db conf
     * @param config applicationConfig
     * @param filename the file to write
     */
    public static void writeFileETLConf(ApplicationConfig config, String filename) {
        File dbConfFile = new File(filename);

            try {
                log.info("Création du fichier de configuration de l'ETL");
                dbConfFile.createNewFile();

                log.info("Ecriture du fichier");
                FileWriter writer;
                writer = new FileWriter(dbConfFile);

                log.info("Ouverture du buffer! Ecriture en cours.....");

                BufferedWriter bw = new BufferedWriter(writer);
                bw.write("# ===============================\n");
                bw.write("# = CONFIGURATION ETL\n");
                bw.write("# ===============================\n");
                bw.write("# = CONFIGURATION OPALE\n");
                bw.write("OPALE_DB_Password=" + config.getPasswordOPALEEncrypted() + "\n");
                bw.write("OPALE_DB_Database=" + config.getDatabaseOPALE() + "\n");
                bw.write("OPALE_DB_Server=" + config.getUrlOPALE() + "\n");
                bw.write("OPALE_DB_Login=" + config.getUsernameOPALE() + "\n");
                bw.write("OPALE_DB_Port=" + config.getPortOPALE() + "\n");
                bw.write("OPALE_DB_AdditionalParams=\n");
                bw.write("OPALE_DB_Schema=" + config.getSchemaOPALE() + "\n");
                bw.write("# = CONFIGURATION ENI\n");
                bw.write("ENI_Password=" + config.getPasswordENIEncrypted() + "\n");
                bw.write("ENI_Database=" + config.getDatabaseENI() + "\n");
                bw.write("ENI_Server=" + config.getUrlENI() + "\n");
                bw.write("ENI_Login=" + config.getUsernameENI() + "\n");
                bw.write("ENI_Port=" + config.getPortENI() + "\n");
                bw.write("ENI_AdditionalParams=\n");
                bw.write("ENI_Schema=" + config.getSchemaENI() + "\n");
                bw.write("# = CONFIGURATION VARIABLE BUILT-IN\n");
                bw.write("OPALE_Habilitations=" + config.getOpaleHabilitationFile() + "\n");
                bw.write("OPALE_Param=" + config.getOpaleParamFile() + "\n");
                bw.write("OPALE_Config=" + config.getOpaleConfigFile() + "\n");
                bw.close();

                log.info("Fermeture du fichier !");
            } catch (IOException e) {
                log.error("Une erreur est survenue : {}", e.getLocalizedMessage());
            }
    }

    /**
     * Write LDAP db conf
     * @param config applicationConfig
     * @param filename the file to write
     */
    public static void writeFileLDAPConf(ApplicationConfig config, String filename) {
        File dbConfFile = new File(filename);

            try {
                log.info("Création du fichier de configuration du LDAP");
                dbConfFile.createNewFile();

                log.info("Ecriture du fichier");
                FileWriter writer;
                writer = new FileWriter(dbConfFile);

                log.info("Ouverture du buffer! Ecriture en cours.....");

                BufferedWriter bw = new BufferedWriter(writer);
                bw.write("# ===============================\n");
                bw.write("# = CONFIGURATION DU LDAP\n");
                bw.write("# ===============================\n");
                bw.write("url=" + config.getUrlLdap() + "\n");
                bw.write("dc=" + config.getDc() + "\n");
                bw.write("dn=" + config.getDn() + "\n");
                bw.close();

                log.info("Fermeture du fichier !");
            } catch (IOException e) {
                log.error("Une erreur est survenue : {}", e.getLocalizedMessage());
            }
    }
}
